import React from "react";

export const Loader = () => {
    return (
        <div className="ui active loader"></div>
    );
};

export default Loader;
